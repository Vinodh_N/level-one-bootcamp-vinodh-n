//WAP to find the sum of two fractions.

#include<stdio.h>

typedef struct
{
    int num ,den;
}fraction;

fraction accept_fraction()
{
    fraction a;
    printf("enter the numerator and denominator of the fraction: ");
    scanf("%d%d",&a.num,&a.den);
    return a;
}
int find_gcd(int x,int y)
{
    int gcd=1,i;
    
    for(i=2;i<=x && i<=y;i++)
    {
        if(x%i==0 && y%i==0)
        {
            gcd=i;
        }
    }
    return gcd;
    
}
fraction compute(fraction sum)
{
    int gcd;
    gcd=find_gcd(sum.num,sum.den);
    sum.num=sum.num/gcd;
    sum.den=sum.den/gcd;
    return sum; 
}

fraction addition(fraction frac1,fraction frac2)
{
    fraction sum;
    sum.num=(frac1.num*frac2.den)+(frac2.num*frac1.den);
    sum.den=(frac1.den*frac2.den);
    sum=compute(sum);
    return sum;
}

void print_sum(fraction frac1,fraction frac2,fraction sum)
{
     printf("The sum of %d/%d and %d/%d = %d/%d",frac1.num,frac1.den,frac2.num,frac2.den,sum.num,sum.den);

 }

int main()
{
    fraction frac1,frac2,sum;
    frac1=accept_fraction();
    frac2=accept_fraction();
    sum=addition(frac1,frac2);
    print_sum(frac1,frac2,sum);
    return 0;
}
