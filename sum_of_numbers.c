//Write a program to find the sum of n different numbers.

#include<stdio.h>

int main()
{
    int a[20],i,n,sum=0;

	printf("How many Numbers you want to add?\n");
	scanf("%d", &n);

	printf("Enter %d Numbers to perform addition:\n",n);

	for(i=0; i<n; i++)
	{
	    scanf("%d", &a[i]);
   	}


   	for (i=0; i<n; i++)
   	{
	    sum = sum + a[i];
    }

    printf("The Sum of all The numbers are: %d\n", sum);
	return 0;
}
