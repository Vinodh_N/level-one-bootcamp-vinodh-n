//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
typedef struct
{
    float x;
    float y;
}point;

point input()
{
    point k;
    printf("enter the values of x and y: ");
    scanf("%f%f",&k.x,&k.y);
    return k;
}

float find_distance(point a,point b)
{
    float distance;
    distance = sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
    return distance;
}

void display(point a,point b,float distance)
{
    printf("the distance between %f,%f and %f,%f is %f", a.x,a.y,b.x,b.y, distance);
}

int main()
{
    float distance;
    point a,b;
    a = input();
    b = input();
    distance = find_distance(a,b);
    display(a,b,distance);
    return 0;
}
